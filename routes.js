var app=require('express');
var routes=app.Router();
var usersController=require('./controller/src/users');
var task = [];
var complete = [];

routes.get("/", function(req, res) {    
  res.render("home", { 
    // task: task,complete:complete 
  });
});

routes.get("/register", function(req, res) {    
  res.render("register", { 
    // task: task,complete:complete 
  });
});
routes.post("/registration",usersController.register);
routes.post("/userLogin",usersController.login);

routes.get("/login", function(req, res) {    
  res.render("login", { 
    // task: task,complete:complete 
  });
});


routes.get("/todo",usersController.todo);

routes.post("/addtask",usersController.addTask);
routes.post("/removeTask",usersController.completeTask);

module.exports = routes ;
// routes.get("/", function(req, res) {    
//   res.render("todo", { task: task,complete:complete,
//     // user_id:user_id
//  });
// });
// routes.post('/addtask', function (req, res) {
//     var newTask = req.body.newtask;
//     task.push(newTask);
//     res.redirect("/");
// });


// routes.post("/removetask", function(req, res) {
//      var completeTask = req.body.check;
//     //  console.log("check ",req.body.check,"completedTask: ",completeTask );
//      complete.push(completeTask);
//     //  console.log("complete: ",complete," length: ",completeTask.length);
//     //   console.log("Before remove task: ",task);
//     //  for(var i=0;i<completeTask.length;i++){
//     //   task.splice(task.indexOf(completeTask),i);
//     //  }
//      task.splice(task.indexOf(completeTask),1);
      
//      console.log("After remove task: ",task);
//     res.redirect("/");
// });

