# Website

**Todo List Website**

First, you have to register yourself and then login. 
Now, you can add and remove your tasks from your to-do list.

**Run App**
1. Download Repo
 
2. Install Node.JS
 
3. Install Dependencies - NPM Install

**Run "node index"**
and browse https://localhost:port

