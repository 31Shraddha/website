var short_id=require('short-id');
var passwordHash=require('password-hash');
var connection=require('./../../index');
var task=[],complete=[];

module.exports.register=function(req,res){
  var today = new Date();
    var users={
        "user_id":short_id.generate(),
        "username":req.body.username,
        "email":req.body.email,
        "password":passwordHash.generate(req.body.password),
        "created_at":today,
        "updated_at":today
    }

    var tasks={
      "user_id":users.user_id,
      "tasks":'',
      "created_at":today,
      "updated_at":today
  }
    connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
      if (error) {
        res.json({
            status:false,
            message:'there are some error with query'
        })
      }else{
        connection.query('INSERT INTO userstask SET ?',tasks, function (error, results, fields) {
          if (error) {
            console.log(error);
            res.json({
                status:false,
                message:'there are some error with query'
            })
          }
          else{
            res.render('todo',{
              status:true,
              message:'successfully authenticated',
              user_id:tasks.user_id,
              task:task,
              complete:complete,
            })                
          
            // res.redirect("/login");
          }
        })
      }
    });
}

module.exports.login=function(req,res){
    var email=req.body.email;
    var password=req.body.password;
    connection.query('SELECT * FROM users WHERE email = ?',[email], function (error, results, fields) {
      if (error) {
          res.json({
            status:false,
            message:'there are some error with query'
            })
      }else{
        var user_id= results[0].user_id;
        if(results.length >0){
            // var storePass=results[0].password;
            var hashedPassword=results[0].password
            if(passwordHash.verify(password, hashedPassword)){
            // if(password==storePass){
              getData(user_id,req,res);
            }else{
                res.json({
                  status:false,
                  message:"Email and password does not match"
                 });
            }
          
        }
        else{
          res.json({
              status:false,    
            message:"Email does not exits"
          });
        }
      }
    });
}

module.exports.todo=function(req, res) {
  var user_id=req.body.user_id;
  getData(user_id,req,res);
 }

 module.exports.addTask=function(req, res) {
    var newTask = req.body.newtask;
    var user_id=req.body.user_id;
      task.push(newTask);
      console.log('add: ',newTask);
        connection.query('INSERT INTO userstask (user_id,tasks) values (?,?)',[user_id,newTask], function (error, results, fields) {
      if (error) {
        console.log(error);
        res.json({
            status:false,
            message:'there are some error with query'
        })
      }else{
        getData(user_id,req,res);
        }
      })

};

module.exports.completeTask=function(req, res) {
  var completeTask = req.body.check;
  var user_id=req.body.user_id;
  complete.push(completeTask);
  task.splice(task.indexOf(completeTask),1);
  connection.query('DELETE from userstask where user_id =? and tasks=?',[user_id,completeTask], function (error, results, fields) {
      if (error) {
        console.log(error);
        res.json({
            status:false,
            message:'there are some error with query'
        })
      }else{
      console.log('success');
      getData(user_id,req,res);
    }
      })
};

function getData(user_id,req,res){
  var t2=[];
  t2.length=0;
  connection.query('SELECT * FROM userstask WHERE user_id = ?',[user_id], function (error, results, fields) {
        if (error) {
          console.log('error: ',error);
            res.json({
              status:false,
              message:'there are some error with query'
              })
        }else{
          for(var i=0;i<results.length;i++){
            t2.push(results[i].tasks);
            console.log('user add tasks',results[i].tasks);
          }
            res.render('todo',{
              status:true,
              message:'successfully authenticated',
              user_id:user_id,
              task:t2,
              complete:complete,
            })                
            }
          });  
  }