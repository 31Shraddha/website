var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var mysql = require('mysql');

var port = process.env.PORT || 3000;
app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'website'
});
connection.connect(function(err){
if(!err) {
    console.log("Database is connected");
} else {
    console.log("Error while connecting with database");
}
});
module.exports = connection; 

var  routes=require('./routes');
app.use('/',routes);


app.listen(port, function () {
  console.log('listening on port 3000!')
});



// "scripts": {
//   "test": "echo \"Error: no test specified\" && exit 1"
// },